import 'package:get_x_hive/src/products/models/category_model.dart';
import 'package:get_x_hive/src/products/models/product_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';

class HiveInit {
  static Future<void> init() async {
    var dir = await getApplicationDocumentsDirectory();
    await Hive.initFlutter(dir.path);
    Hive.registerAdapter(CategoryModelAdapter());
    Hive.registerAdapter(ProductModelAdapter());
    print('Init hive');
  }
}
