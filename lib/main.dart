import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x_hive/configs/router.dart';

import 'core/utils/hive_init.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await HiveInit.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/products',
      getPages: AppRoutes.routes,
    );
  }
}
