import 'package:get/get.dart';
import 'package:get_x_hive/src/products/controllers/product_controller.dart';
import 'package:get_x_hive/src/products/providers/product_provider.dart';

class ProductBinding extends Bindings {
  @override
  void dependencies() {
    // TODO: implement dependencies
    Get.put(ProductProvider());
    Get.put(ProductController());
  }
}
