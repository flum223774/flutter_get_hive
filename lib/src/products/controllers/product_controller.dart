import 'package:get/get.dart';
import 'package:get_x_hive/src/products/models/product_model.dart';
import 'package:get_x_hive/src/products/providers/product_provider.dart';

class ProductController extends GetxController {
  ProductProvider _provider = Get.find();
  RxList<ProductModel> _product = RxList([]);
  RxBool _isLoading = RxBool(false);

  bool get isLoading => _isLoading.value;

  List<ProductModel> get products => _product;

  Future<void> _getProducts() async {
    _product(await _provider.getProducts());
  }

  @override
  void onInit() {
    super.onInit();
    _getProducts();
  }

  addProduct(ProductModel model) {
    _product.add(model);
  }

  updateProduct(ProductModel model) {
    var index = _product.indexWhere((element) => element.id == model.id);
    if (index > -1) {
      _product[index] = model;
    }
  }

  Future<void> deleteProduct(ProductModel model) async {
    var isSave = await _provider.deleteProduct(model);
    if (isSave) {
      _product.removeWhere((element) => element.key == model.key);
    }
  }
}
