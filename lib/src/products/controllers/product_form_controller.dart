import 'package:get/get.dart';
import 'package:get_x_hive/src/products/controllers/product_controller.dart';
import 'package:get_x_hive/src/products/models/category_model.dart';
import 'package:get_x_hive/src/products/models/product_model.dart';
import 'package:get_x_hive/src/products/providers/product_provider.dart';

class ProductFormController extends GetxController {
  ProductController _productController = Get.find();
  RxList<CategoryModel> _categories = RxList([]);

  ProductProvider _productProvider = Get.find();
  RxBool isEditMode = RxBool(false);

  Rx<ProductModel> _product = Rx<ProductModel>(ProductModel.init());

  ProductModel get product => _product.value;

  set product(ProductModel productModel) {
    _product(productModel);
  }

  List<CategoryModel> get categories => _categories;

  @override
  onInit() {
    super.onInit();
    var arguments = Get.arguments;
    print(arguments);
    if (arguments != null) {
      isEditMode(arguments['isEditMode']);
      _product(arguments['model'] as ProductModel);
      print(_product.value.id);
    }
  }

  Future<void> saveProduct() async {
    if (isEditMode.value) {
      ProductModel productNew =
          await _productProvider.updateProduct(product);
      _productController.updateProduct(productNew);
    } else {
      ProductModel productNew = await _productProvider.addProduct(product);
      _productController.addProduct(productNew);
    }
    Get.back();
  }
}
