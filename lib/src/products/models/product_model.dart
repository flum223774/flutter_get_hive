import 'package:hive/hive.dart';
part 'product_model.g.dart';
@HiveType(typeId: 1)
class ProductModel extends HiveObject {
  static String table = 'products';
  @HiveField(1)
  int? id;
  @HiveField(2)
  String productName;
  @HiveField(3)
  int? categoryId;
  @HiveField(4)
  String productDesc;
  @HiveField(5)
  double? price;
  @HiveField(6)
  String? productPic;

  ProductModel({
    this.id,
    required this.productName,
    this.categoryId,
    required this.productDesc,
    required this.price,
    this.productPic,
  });

  static ProductModel init() {
    return ProductModel(productName: '', productDesc: '', price: 0);
  }

  ProductModel copyWith(
      {String? productName,
      int? categoryId,
      String? productDesc,
      double? price,
      String? productPic}) {
    return ProductModel(
      id: this.id,
      productName: productName ?? this.productName,
      categoryId: categoryId ?? this.categoryId,
      productDesc: productDesc ?? this.productDesc,
      price: price ?? this.price,
      productPic: productPic ?? this.productPic,
    );
  }

  static ProductModel fromMap(Map<String, dynamic> map) {
    return ProductModel(
      id: map["id"],
      productName: map['productName'].toString(),
      categoryId: map['categoryId'],
      productDesc: '${map['productDesc']}',
      price: map['price'],
      productPic: map['productPic'],
    );
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      'id': id,
      'productName': productName,
      'categoryId': categoryId,
      'productDesc': productDesc,
      'price': price,
      'productPic': productPic
    };

    if (id != null) {
      map['id'] = id;
    }
    return map;
  }
}
