import 'package:get_x_hive/src/products/models/product_model.dart';
import 'package:get_x_hive/src/products/models/category_model.dart';
import 'package:hive/hive.dart';

class ProductProvider {
  Future<List<ProductModel>> getProducts() async {
    Box box = await Hive.openBox<ProductModel>('products');
    List<ProductModel> products = box.values.toList() as List<ProductModel>;
    box.close();
    return products;
  }

  Future<ProductModel> addProduct(ProductModel productModel) async {
    Box box = await Hive.openBox<ProductModel>('products');
    var key = await box.add(productModel);
    ProductModel newProduct = box.get(key);
    box.close();
    return newProduct;
  }

  Future<ProductModel> updateProduct(ProductModel product) async {
    Box box = await Hive.openBox<ProductModel>('products');
    await box.put(
        product.key,
        ProductModel(
            productName: product.productName,
            price: product.price,
            productDesc: product.productDesc,
            productPic: product.productPic));
    ProductModel newP = box.get(product.key);
    box.close();
    return newP;
  }

  Future<bool> deleteProduct(ProductModel model) async {
    Box box = await Hive.openBox<ProductModel>('products');
    box.delete(model.key);
    box.close();
    return true;
  }

  Future<List<CategoryModel>?> getCategories() async {}
}
