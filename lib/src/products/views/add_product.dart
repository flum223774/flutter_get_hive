import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_x_hive/core/utils/form_helper.dart';
import 'package:get_x_hive/src/products/controllers/product_form_controller.dart';
import 'package:get_x_hive/src/products/models/category_model.dart';

class ProductForm extends GetView<ProductFormController> {
  const ProductForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.redAccent,
        automaticallyImplyLeading: true,
        title:
            Text(controller.isEditMode.value ? "Edit Product" : "Add Product"),
      ),
      body: _formUI(context),
    );
  }

  Widget _formUI(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          child: Align(
            alignment: Alignment.topLeft,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                FormHelper.fieldLabel("Product Name"),
                FormHelper.textInput(
                  context,
                  controller.product.productName,
                  (value) => controller.product.productName = value,
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter Product Name.';
                    }
                    return null;
                  },
                ),
                FormHelper.fieldLabel("Product Description"),
                FormHelper.textInput(context, controller.product.productDesc,
                    (value) => controller.product.productDesc = value,
                    isTextArea: true, onValidate: (value) {
                  return null;
                }),
                FormHelper.fieldLabel("Product Price"),
                FormHelper.textInput(
                  context,
                  controller.product.price,
                  (String value) {
                    if (GetUtils.isNum(value)) {
                      controller.product.price = double.parse(value);
                    } else {
                      controller.product.price = 0;
                    }
                  },
                  isNumberInput: true,
                  onValidate: (value) {
                    if (value.toString().isEmpty) {
                      return 'Please enter price.';
                    }

                    if (value.toString().isNotEmpty &&
                        double.parse(value.toString()) <= 0) {
                      return 'Please enter valid price.';
                    }
                    return null;
                  },
                ),
                FormHelper.fieldLabel("Product Category"),
                Obx(() => _productCategory(context)),
                FormHelper.fieldLabel("Select Product Photo"),
                FormHelper.picPicker(
                  controller.product.productPic,
                  (file) => controller.product.productPic = file.path,
                ),
                btnSubmit(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _productCategory(BuildContext context) {
    return FormHelper.selectDropdown(
      context,
      controller.product.categoryId,
      controller.categories,
      (value) => {controller.product.categoryId = int.parse(value)},
      onValidate: (value) {
        if (value == null) {
          return 'Please enter Product Category.';
        }
        return null;
      },
    );
    // return FutureBuilder<List<CategoryModel>>(
    //   future: controller.getCategories(),
    //   builder: (BuildContext context,
    //       AsyncSnapshot<List<CategoryModel>> categories) {
    //     if (categories.hasData) {
    //       return FormHelper.selectDropdown(
    //         context,
    //         controller.categoryId,
    //         categories.data,
    //         (value) => {controller.categoryId = int.parse(value)},
    //         onValidate: (value) {
    //           if (value == null) {
    //             return 'Please enter Product Category.';
    //           }
    //           return null;
    //         },
    //       );
    //     }
    //
    //     return CircularProgressIndicator();
    //   },
    // );
  }

  // bool validateAndSave() {
  //   final form = controller.globalFormKey.currentState;
  //   if (form != null && form.validate()) {
  //     form.save();
  //     return true;
  //   }
  //   return false;
  // }

  Widget btnSubmit() {
    return new Align(
      alignment: Alignment.center,
      child: InkWell(
        onTap: controller.saveProduct,
        child: Container(
          height: 40.0,
          margin: EdgeInsets.all(10),
          width: 100,
          color: Colors.blueAccent,
          child: Center(
            child: Text(
              "Save Product",
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
